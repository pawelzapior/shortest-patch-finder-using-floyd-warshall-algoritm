# -*- coding: utf-8 -*-
"""
@author: Paweł Zapiór
"""
import math

def destinationArray():
    try:
        file=open("dane.txt", "r")
        tab_wiersz=file.readlines()
        mac =[]
        for i in tab_wiersz:
            i=i.replace("\n", "")
            mac.append(i.split(" "))
        for i in range(0,len(mac)):
            for j in range(0,len(mac)):
                mac[i][j]=int(mac[i][j])
        file.close()
        return mac
    except:
        print("File reading error.")
             
    
def city(s):
    if(s==0):
        return "Kraków"
    if(s==1):
        return "Katowice"
    if(s==2):
        return "Wroclaw"
    if(s==3):
        return "Poznan"
    if(s==4):
        return "Szczecin"
    if(s==5):
        return "Gdansk"
    if(s==6):
        return "Warszawa"
    if(s==7):
        return "Bialystok"
    if(s==8):
        return "Lublin"

def printroad(road,price):
    print("\n Aby wybudowac droge z  ", city(road[0]), " do ", city(road[-1]), " poprowadz ja najlepiej przez nastepujace miasta: ")
    for i in range(1, len(road)-1):
        print(city(road[i]))
    print("I osiągnij cel: ", city(road[-1]))
    print("Cena budowy tej drogi wyniesie: ", price)    

def floyd_warshall(mac):
    d=[]
    p=[]
    mx=len(mac) 
    miasta=[]
    
# na podstawie rozmiarów macierzy tworzę tablice zawierającą numery kolejnych wierzchołków
    
    for i in range(1,mx+1):
        miasta.append(i)
    
    
# Definiowanie tablic początkowych
    for i in range(0,mx):
    	d += [[math.inf]*(mx)]
    	p += [[0]*(mx)]
    	d[i][i] = 0 
    	p[i][i] = 0 
        
        

    for i in range(len(miasta)):
        for j in range(len(miasta)):
            if i==j:
                d[i][j] = 0
            elif mac[i][j] == 0:
                d[i][j] = math.inf
            else:
                d[i][j] = mac[i][j]

    for i in range(len(miasta)):
        for j in range(len(miasta)):
            if mac[i][j] != 0:
                p[i][j]=miasta[i]
            else:
                p[i][j] = 0
                                

    for u in miasta:
        u = u - min(miasta)
        for v in miasta:
            v = v - min(miasta)

            if v != u:
                for w in miasta:
                    w = w - min(miasta)

                    if w != u and w != v:
                        x = d[v][u] + d[u][w]

                        if x < d[v][w]:
                            d[v][w] = x
                            p[v][w] = p[u][w]


    for source in range (0,mx):
        for destination in range (0,mx):            
            if(source!=destination):
                 road = []
                 road += [source]
                 actpoint = source
                 while (p[destination][actpoint] != destination+1):
                     road += [p[destination][actpoint]-1]
                     actpoint=p[destination][actpoint]-1
                 
                 price=d[destination][source]    
                 road += [destination]
                        
                        
         
                 
                 printroad(road,price)
                
                 print("************************************************************************************************")

    


mac=destinationArray()
floyd_warshall(mac)


